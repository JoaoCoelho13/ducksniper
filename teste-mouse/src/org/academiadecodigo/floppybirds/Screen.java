package org.academiadecodigo.floppybirds;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Screen {

    Rectangle background;

    public Screen() {
        background = new Rectangle(10, 10, 1000, 800);
        background.draw();
    }

    public void draw(double x, double y) {
        Rectangle test = new Rectangle(x, y - 23, 10, 10);
        test.draw();
    }
}