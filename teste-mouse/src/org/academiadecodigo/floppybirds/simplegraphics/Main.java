package org.academiadecodigo.floppybirds.simplegraphics;

import org.academiadecodigo.floppybirds.Controls;
import org.academiadecodigo.floppybirds.Screen;


public class Main {
    public static void main(String[] args) {
        Screen screen = new Screen();
        Controls controls = new Controls();
        controls.init(new Screen());
    }
}
