package org.academiadecodigo.floppybirds;
import org.academiadecodigo.simplegraphics.mouse.Mouse;
import org.academiadecodigo.simplegraphics.mouse.MouseEvent;
import org.academiadecodigo.simplegraphics.mouse.MouseEventType;
import org.academiadecodigo.simplegraphics.mouse.MouseHandler;

public class Controls implements MouseHandler {

    private Mouse mouse;
    private Screen screen;

    public void init(Screen screen) {

        mouse = new Mouse(this);
        mouse.addEventListener(MouseEventType.MOUSE_CLICKED);
        this.screen = screen;
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

        System.out.println("move");

    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

        screen.draw(mouseEvent.getX(), mouseEvent.getY());
        System.out.println("x: " + mouseEvent.getX() + " y: " + mouseEvent.getY());

    }

}