package org.academiadecodigo.floppybirds;

import org.academiadecodigo.floppybirds.elements.Element;
import org.academiadecodigo.floppybirds.elements.ElementFactory;

import java.util.LinkedList;

public class Game {

    private Background background;
    private Sniper sniper;
    private int delay;

    private Element[] elementsArray;
    private int counter = 0;
    private int counterImages = 1;
    private int numberOfElements = 20;


    public Game(Background background, Sniper sniper, int delay) {
        this.background = background;
        this.sniper = sniper;
        this.delay = delay;
    }

    public void init() {
        sniper.show();
        background.show();

        elementsArray = new Element[numberOfElements];

        for (int i = 0; i < numberOfElements; i++) {
            elementsArray[i] = ElementFactory.getNewElement(background);
        }
    }

    public void start() throws InterruptedException {

        while(true) {

            Thread.sleep(delay);

            //creates the Elements to shoot
            //createElements();

            //defines the sniper target
            //sniperTargetAttribution();    ACHO QUE NAO VAI SER PRECISO

            //makes the Elements move
            moveElements();


        }
    }

    /*public void createElements() {

        if (counter == 9) {
            if (elementsLinkedlist.size() > 3) {

                Element[] elementsArray = elementsLinkedlist.toArray(new Element[elementsLinkedlist.size()]);

                counter = 0;
                return;
            }
            elementsLinkedlist.add(ElementFactory.getNewElement(background));
            counter = 0;
        }

        counter++;
    }*/

    public void moveElements() {

        if (counterImages != elementsArray.length) {

            if (counter % 15 == 0) {
                counterImages++;
            }
        }

        for (int i = 0; i < counterImages; i++) {
            elementsArray[i].move();
            elementsArray[i].show();
            counter++;
        }
    }
}
