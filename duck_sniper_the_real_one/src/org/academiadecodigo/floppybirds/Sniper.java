package org.academiadecodigo.floppybirds;

import org.academiadecodigo.floppybirds.elements.Element;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Sniper {

    private Picture sniper;
    private Background background;

    public Sniper(Background background) {
        sniper = new Picture(background.getWidth() / 2, background.getHeight(), "littledog.jpg");
        this.background = background;
    }

    public void show() {
        sniper.draw();
    }

    public void shoot(double hitBoxX, double hitBoxY) {

    }

}
