package org.academiadecodigo.floppybirds.elements;

import org.academiadecodigo.floppybirds.Background;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Friend extends Element {

    public Friend(Background background) {
        super(new Picture(background.PADDING, background.getHeight() / 3, "sangokus.png"));
    }


}
