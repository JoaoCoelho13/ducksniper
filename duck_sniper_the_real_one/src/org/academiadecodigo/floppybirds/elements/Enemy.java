package org.academiadecodigo.floppybirds.elements;

import org.academiadecodigo.floppybirds.Background;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Enemy extends Element{

    public Enemy(Background background) {
        super(new Picture(background.PADDING, background.getHeight() / 3, "pipininopequenino.png"));
    }
}
