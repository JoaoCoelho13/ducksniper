package org.academiadecodigo.floppybirds.elements;

import org.academiadecodigo.simplegraphics.pictures.Picture;

import java.util.Queue;

public abstract class Element {

    private boolean isDead;
    private boolean wentUp;

    private int imageFinalX;
    private int imageStartX;
    private int imageHeight;

    private Picture element;

    public Element(Picture element) {
        this.element = element;
        imageStartX = element.getX();
        imageFinalX = imageStartX + element.getWidth();
        imageHeight = element.getY();

        wentUp = false;
    }

    public void move() {

        if (imageFinalX >= 900) {
            //delete();
            if (!wentUp) {
                element.translate(-700,5);
                wentUp = true;

            }

            element.translate(-700, -5);
            wentUp = false;
        }


        int random = element.getWidth() + (int) Math.ceil(Math.random() * 4);


        element.translate(random, 0);

        imageStartX = element.getX();
        imageFinalX = imageStartX + element.getWidth();
    }

    public void show() {
        if(!isDead) {
            element.draw();
        }
    }

    //Possivelmente utilizar
    public void delete() {
        element.delete();
    }

    public void isDead() {
        isDead = true;
    }

    public int getX() {
        return imageStartX;
    }

    public int getWidth() {
        return imageFinalX;
    }

    public void setNewPosition() {
        element.translate(-800,30);
    }
}
