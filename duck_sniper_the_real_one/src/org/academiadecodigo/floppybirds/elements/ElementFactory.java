package org.academiadecodigo.floppybirds.elements;

import org.academiadecodigo.floppybirds.Background;

public class ElementFactory {

    public static Element getNewElement(Background background) {

        Element element;

        if(Math.random() < 0.5) {
            element = new Enemy(background);
        } else {
            element = new Friend(background);
        }

        return element;
    }
}
