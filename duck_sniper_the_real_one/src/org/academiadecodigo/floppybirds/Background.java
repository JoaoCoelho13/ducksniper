package org.academiadecodigo.floppybirds;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Background {

    private Picture background;
    private int width;
    private int height;
    public static final int PADDING = 10;

    public Background() {
        background = new Picture(PADDING, PADDING, "backgroundmaisfixe.png");
        width = background.getWidth();
        height = background.getHeight();
    }

    public void show() {
        background.draw();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void hitBoxDraw(double x, double y) {
        Rectangle test = new Rectangle(x, y - 23, 10, 10);
        test.draw();
    }
}
