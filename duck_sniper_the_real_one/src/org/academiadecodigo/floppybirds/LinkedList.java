package org.academiadecodigo.floppybirds;

import java.util.Iterator;

public class LinkedList <T> implements Iterable<T>{

    private Node head;
    private int length = 0;

    public LinkedList() {
        head = new Node(null);
    }

    public int size() {
        return length;
    }

    /**
     * Adds an element to the end of the list
     * @param data the element to add
     */
    public void add(T data)  {

        Node node = new Node(data);
        Node iterator = head;
        while (iterator.getNext() != null){
            iterator = iterator.getNext();
        }
        iterator.setNext(node);
        length++;

    }

    public void addAt(int index, T data) {

        Node node = new Node(data);
        Node iterator = head;

        if(index == 0) {
            Node placeHolder = iterator.getNext();
            iterator.setNext(node);
            node.setNext(placeHolder);

            length++;
            return;
        }

        for (int i = 0; i < length; i++) {
            if (i == index - 1) {
                Node placeHolder = iterator.getNext();
                iterator.setNext(node);
                node.setNext(placeHolder);

                length++;
                return;
            }
            iterator = iterator.getNext();
        }
        return;
    }

    /**
     * Obtains an element by index
     * @param index the index of the element
     * @return the element
     */
    public T get(int index) {

        Node iterator = head.getNext();

        for (int i = 0; i < length; i++) {
            if (i == index) {
                return iterator.getData();
            }
            iterator = iterator.getNext();
        }

        return null;
    }


    /**
     * Returns the index of the element in the list
     * @param data element to search for
     * @return the index of the element, or -1 if the list does not contain element
     */
    public int indexOf(T data) {

        Node iterator = head.getNext();

        for (int i = 0; i < length; i++) {
            if (iterator.getData() == data) {
                return i;
            }
            iterator = iterator.getNext();
        }

        return -1;
    }

    /**
     * Removes an element from the list
     * @param data the element to remove
     * @return true if element was removed
     */
    public boolean removeByData(T data) {

        Node previousIterator = head;
        Node currentIterator = head.getNext();

        for (int i = 0; i < length; i++) {

            if (currentIterator.getData().equals(data)) {

                previousIterator.setNext(currentIterator.getNext());
                length --;
                return true;
            }
            previousIterator = currentIterator;
            currentIterator = currentIterator.getNext();
        }

        return false;
    }

    public boolean removeByIndex(int index) {

        Node previousIterator = head;
        Node currentIterator = head.getNext();

        for (int i = 0; i < length; i++) {
            if (i == index) {
                previousIterator.setNext(null);
                length--;
                return true;
            }

            previousIterator = currentIterator;
            currentIterator = currentIterator.getNext();
        }

        return false;
    }

    public T getElement(int index) {

        Node iterator = head.getNext();

        for (int i = 0; i < length; i++) {
            if (i == index) {
                iterator.setNext(null);
                return iterator.getData();
            }
            iterator = iterator.getNext();
        }

        return iterator.getData();

    }

    @Override
    public Iterator<T> iterator() {
        return new MyIterator();
    }

    private class Node {

        private T data;
        private Node next;

        public Node(T data) {
            this.data = data;
            next = null;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    private class MyIterator implements Iterator<T> {

        private Node current;

        public MyIterator() {
            current = head;
        }

        @Override
        public boolean hasNext() {
            return (current.getNext() != null);
        }

        @Override
        public T next() {
            current = current.getNext();
            return current.getData();
        }
    }
}
