package org.academiadecodigo.floppybirds;

import org.academiadecodigo.simplegraphics.mouse.Mouse;
import org.academiadecodigo.simplegraphics.mouse.MouseEvent;
import org.academiadecodigo.simplegraphics.mouse.MouseHandler;
import org.academiadecodigo.simplegraphics.mouse.MouseEventType;


public class Controller implements MouseHandler {

    private Mouse mouse;
    private Background background;

    private Sniper sniper;

    public void init(Background background, Sniper sniper) {

        mouse = new Mouse(this);
        mouse.addEventListener(MouseEventType.MOUSE_CLICKED);
        this.background = background;
        this.sniper = sniper;
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

        //Acrescentar mira
        System.out.println("move");

    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

        double hitBoxX = mouseEvent.getX();
        double hitBoxY = mouseEvent.getY();
        //sniper.shoot(hitBoxX, hitBoxY);

        background.hitBoxDraw(mouseEvent.getX(), mouseEvent.getY());
        System.out.println("x: " + mouseEvent.getX() + " y: " + mouseEvent.getY());

    }
}
