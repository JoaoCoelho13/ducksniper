package org.academiadecodigo.floppybirds.duckSniper;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Background {

    private Picture background;
    private int maxWidth;
    private int maxHeight;
    private final int PADDING = 10;


    public Background () {
        background = new Picture(PADDING, PADDING, "background.jpg"); // Add image of background. 800x600 or 1280x800
        this.maxWidth = background.getWidth();
        this.maxHeight = background.getHeight();
        background.draw();
        // Ver como fixar janela.

    }

    public int getMaxWidth() {
        return maxWidth;
    }

    public int getMaxHeight() {
        return maxHeight;
    }

}
